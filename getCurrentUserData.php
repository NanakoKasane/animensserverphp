<?php
header("Content-Type: application/json; charset=UTF-8");
include ("dao.php");

$dao = new DAO();

if ($_GET){
    if ( isset($_GET["email"]) ){
        $email = $_GET["email"];
        
        $result = $dao -> getUserFromEmail($email);
        $columns = $result -> fetchAll();

        if (count($columns) > 0){
            $response["profile"] = array();
        
            foreach($columns as $profiletmp){
                $profile = array();  

                $profile["name"] = $profiletmp["name"];
                $profile["email"] = $profiletmp["email"];
                $profile["nick"] = $profiletmp["nick"];
                $profile["picture"] = $profiletmp["picture"];

                // como un add
                array_push($response["profile"], $profile);
            }
        
        
            echo json_encode($response, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
        }

    }


}



?>