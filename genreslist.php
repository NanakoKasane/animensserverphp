<?php
header("Content-Type: application/json; charset=UTF-8");
include ("dao.php");

$dao = new DAO();

$resultset = $dao -> getGenresList();
$columns = $resultset -> fetchAll();

if (count($columns) > 0){
    $response["anime_genres"] = array();

    foreach($columns as $anime_genretmp){
        $anime_genre = array();  
        $anime_genre['anime_id'] = $anime_genretmp['anime_id'];
        $anime_genre["genre"] = $anime_genretmp["genre"];

        // como un add
        array_push($response["anime_genres"], $anime_genre);
    }


    echo json_encode($response, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);

}

?>