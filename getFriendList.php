<?php
header("Content-Type: application/json; charset=UTF-8");
include ("dao.php");
$dao = new DAO();

if ($_GET){
    if (isset($_GET["nick"])){
        $nick = $_GET["nick"];

        $users = $dao -> getFriendList($nick);
        showUsers($users -> fetchAll());
    }
}

function showUsers($columns){
    $users["users"] = array(); 

    if (count($columns) > 0){
        for($i = 0; $i < count($columns); $i++){
            $user = array();
            $user["nick"] = $columns[$i]["nick"];
            $user["name"] = $columns[$i]["name"];
            $user["picture"] = $columns[$i]["picture"];

            array_push($users["users"], $user);

        }    
        echo json_encode($users, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);

    }
    else{ // Vacío
        $users["users"] = array(); 
        echo json_encode($users, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
    }
}
