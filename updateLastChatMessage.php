<?php
header("Content-Type: application/json; charset=UTF-8");
include ("dao.php");

$dao = new DAO();

if ($_POST){
    if (isset($_POST["lastMessage"]) && isset($_POST["timeLastMessage"]) && isset($_POST["dateLastMessage"]) && isset($_POST["senderLastMessage"]) && isset($_POST["chat_id"]) ){
        $lastMessage = $_POST["lastMessage"];
		$timeLastMessage = $_POST["timeLastMessage"];
		$dateLastMessage = $_POST["dateLastMessage"];
		$senderLastMessage = $_POST["senderLastMessage"];
		$chat_id = $_POST["chat_id"];
		
        $dao -> updateLastChatMessage($lastMessage, $timeLastMessage, $dateLastMessage, $senderLastMessage, $chat_id);
        echo "updated";
    }
}


?>