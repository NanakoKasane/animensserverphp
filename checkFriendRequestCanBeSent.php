<?php
header("Content-Type: application/json; charset=UTF-8");
include ("dao.php");

$dao = new DAO();

if ($_GET){
    if (isset($_GET["nick1"]) && isset($_GET["nick2"])){
        // Deben probarse las 3 combinaciones. Uno como userwhosends y al revés
        $nick1 = $_GET["nick1"];
        $nick2 = $_GET["nick2"];



        if ($dao -> checkFriendRequestCanBeSent($nick1, $nick2)){
            // SE PUEDEN MANDAR
           // echo "Se puede mandar petición"; // MAYBE UN JSON MEJOR, que sea con un dato TRUE o FALSE
            $response["canBeSent"] = true;
        }else{
            $response["canBeSent"] = false;
        }

        echo json_encode($response, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);

    }
}

?>