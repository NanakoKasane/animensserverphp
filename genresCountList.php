<?php
header("Content-Type: application/json; charset=UTF-8");
include ("dao.php");

$dao = new DAO();

$resultset = $dao -> getGenresCountList();
$columns = $resultset -> fetchAll();

if (count($columns) > 0){
    $response["genres"] = array();

    foreach($columns as $genretmp){
        $genre = array();  
        $genre['count'] = $genretmp['count(*)'];
        $genre["genre"] = $genretmp["genre"];

        // como un add
        array_push($response["genres"], $genre);
    }


    echo json_encode($response, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);

}

?>