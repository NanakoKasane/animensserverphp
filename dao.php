<?php
define ("DATABASE", "anime2");
define ("DSN", "mysql:host=localhost;dbname=" . DATABASE . ";charset=utf8");
define ("USER", "www-data");
define ("PASSWORD", "www-data");

define ("TABLE_NAME", "anime3");

class DAO{
    private $conn;
    public $error;

    function __construct(){
        try{
            $this -> conn = new PDO(DSN, USER, PASSWORD);
            $this -> conn ->exec("set names utf8");
        }
        catch(PDOException $e){
            $this -> error = $e -> getMessage() ;
        }
    }

    function getAnimes(){
        return $this -> conn -> query("select * from " . TABLE_NAME . " order by rating desc limit 2000");
    } 

    function getAnimeOfGenre($genre){
        $sql = "select * from anime3 where anime_id in (select anime_id from anime_genres where genre = '$genre') order by rating desc;";
        return $this -> conn -> query($sql);
    }

    function getFavoritesAnimes($nick){
        return $this -> conn -> query("select * from anime3 where anime_id in (select anime_id from anime_user where user_id = (select user_id from user where nick = '$nick') and rating_useranime >= 5.0 ) ");
    }

    function getConn(){
        return $this -> conn;
    }

    function getGenresCountList(){
        return $this -> conn -> query("select genre, count(*) from anime_genres group by genre");
    }

    function getGenresList(){
        return $this -> conn -> query("select genre, anime_id from anime_genres");
    }

    function getGenresStringList(){
        return $this -> conn -> query("select id, genre from genres");
    }

    function getIfUserExists($nick, $password){
        $sql = "select count(*) from user where nick = '" . $nick . "' and  passwordEncrypted = '" . $password . "'";
        return $this -> conn -> query($sql);
    }


    function getIfPhoneExists($phone){
        $sql = "select count(*) from user where phonenumber = '" . $phone . "'";
        return $this -> conn -> query($sql);
    }

    function getIfEmailExists($email){
        $sql = "select count(*) from user where email = '" . $email . "'";
        return $this -> conn -> query($sql);
    }

    function getIfNickExists($nick){
        $sql = "select count(*) from user where nick = '" . $nick . "'";
        return $this -> conn -> query($sql);
    }

    function insertUser($nick, $email, $password){ //  $phone, 
        $sql = "SELECT MAX(CAST(user_id AS SIGNED)) FROM user";
        $columns = $this -> conn -> query($sql) -> fetchAll();
        $maxId = $columns[0]["MAX(CAST(user_id AS SIGNED))"];
        $maxId++;
        // echo $maxId;

        try{
            $sqlInsert = "insert into user values (?, ?, ?, ?, ?, ?, ?)";
            $statement = $this -> conn -> prepare($sqlInsert);
            //if (empty($phone))
            //    $phone = null;
            // if (empty($email))
            //    $email = null;

            //$image = this.getImageRandom(); // "https://i.gyazo.com/786bc10036ce917ea603c1cddeef7f81.png"
            //echo $image;
            $imagesDir = '/home/marina/animens/imagenes/rndprofile/';
            $images = glob($imagesDir . '*.{jpg,jpeg,png,gif}', GLOB_BRACE);
            $randomImage = $images[array_rand($images)]; 
            $randomImageUrl = "https://marina.edufdezsoy.es/" . substr($randomImage, 13);
            echo $randomImageUrl;

            $statement -> execute(array($maxId, $nick, $password, null, null, $email, $randomImageUrl)); // EL ULTIMO ES PICTURE Y NO PUEDE SER NULL
        }
        catch(PDOException $e){
            echo $e -> getMessage();
        }
    }

    function getAllUsers(){
        $sql = "SELECT nick, name, picture from user";
        return $this -> conn -> query($sql);
    }

    function getUsersOrderByAnimeFavorites($nick){
        $sqlCommon = "select distinct nick, name, picture from user where user_id in 
            (select user_id from (select user_id, count(user_id) as animesEnComun from anime_user where anime_id in 
                (select anime_id from anime_user where user_id = (select user_id from user where nick = '$nick') 
                    and rating_useranime >= 5.0  ) and user_id != (select user_id from user where nick = '$nick') 
                            group by user_id order by animesEnComun desc) as t) ";
		
        $sqlCommonNoFav = "select distinct nick, name, picture from user where user_id in 
            (select user_id from (select user_id, count(user_id) as animesEnComun from anime_user where anime_id in 
                (select anime_id from anime_user where user_id = 
                    (select user_id from user where nick = '$nick') and rating_useranime < 5.0  ) and user_id != 
                        (select user_id from user where nick = '$nick') group by user_id order by animesEnComun desc) as t) ";

        $sqlNoCommon = "select nick, name, picture from user where user_id in 
            (select user_id from (select user_id, 0 as animesEnComun from anime_user where anime_id not in 
                (select anime_id from anime_user where user_id not in (select user_id from user where nick = '$nick')  )
                    and user_id != (select user_id from user where nick = '$nick') group by user_id order by animesEnComun desc) as t) ";

        $sqlNoAnimeFavorites = "select distinct nick, name, picture from user where user_id not in 
            (select user_id from anime_user) and nick != '$nick'";

        $response["users"] = array();

        // Primera consulta (obtiene los usuarios que tienen animes en común)
        $resultCommon = $this -> conn -> query($sqlCommon);
        $columnsCommon = $resultCommon -> fetchAll();
        if (count($columnsCommon) > 0){
            foreach($columnsCommon as $usertmp){
                $user = array();  

                $user["nick"] = $usertmp["nick"];
                $user["name"] =$usertmp["name"];
                $user["picture"] = $usertmp["picture"];

                array_push($response["users"], $user);
            }
        }
       
        // 1.1 Consulta (obtiene los que tienen animes en común pero no favoritos)
		$resultCommonNoFav = $this -> conn -> query($sqlCommonNoFav);
        $columnsCommonNoFav = $resultCommonNoFav -> fetchAll();
        if (count($columnsCommonNoFav) > 0){
            foreach($columnsCommonNoFav as $usertmp){
                $user = array();  

                $user["nick"] = $usertmp["nick"];
                $user["name"] =$usertmp["name"];
                $user["picture"] = $usertmp["picture"];

                array_push($response["users"], $user);
            }
        }


        // Segunda consulta (obtiene los que no tienen en común)
        $resultNoCommon = $this -> conn -> query($sqlNoCommon);
        $columnsNoCommon = $resultNoCommon -> fetchAll();
        if (count($columnsNoCommon) > 0){
            foreach($columnsNoCommon as $usertmp){
                $user = array();  

                $user["nick"] = $usertmp["nick"];
                $user["name"] =$usertmp["name"];
                $user["picture"] = $usertmp["picture"];

                array_push($response["users"], $user);
            }
        }
    


        // Tercera consulta (obtiene los que no tienen siquiera animes añadidos como favoritos)
            $sqlNoAnimeFavorites = "select distinct nick, name, picture from user where nick != '$nick'"; // Si no hay ninguno, hay que obtenerlos todos
        $result = $this -> conn -> query($sqlNoAnimeFavorites);
        $columns = $result -> fetchAll();
        if (count($columns) > 0){
            foreach($columns as $usertmp){
                $user = array();  

                $user["nick"] = $usertmp["nick"];
                $user["name"] =$usertmp["name"];
                $user["picture"] = $usertmp["picture"];
                
                $exists = false;
                foreach($response["users"] as $usernew){
                    if ($usernew["nick"] == $user["nick"])
                        $exists = true;
                }
                if (!$exists)
                    array_push($response["users"], $user);
            }
        }


        return $response;
    }

    // Creo las salas de chat
    function createChatRooms($nick, $users, $picture){
        foreach($users["users"] as $user){
            $nicktmp = $user["nick"];
            $imagetmp = $user["picture"];

            $sql = "insert into chatroom (nick1, nick2, picture_nick1, picture_nick2, isReaded) values ('$nick', '$nicktmp', '$picture', '$imagetmp', 0)";
            $this -> conn -> query($sql);
        }

        return 0;
    }

    function getAllChatRoom($nick){
        $response = array();
        $response["chatroom"] = array();

        // TODO:
        $sql = "select * from chatroom where nick1 = '$nick' or nick2 = '$nick';";

        //echo $sql;
        $result = $this -> conn -> query($sql);
        //$response = $this -> createArrayChatRoom($result, $response);
        $columns = $result -> fetchAll();
        if (count($columns) > 0){
            foreach($columns as $chatroomtmp){
                $chatroom = array();  

                $chatroom["chat_id"] = $chatroomtmp["chat_id"];
                $chatroom["nick1"] = $chatroomtmp["nick1"];
                $chatroom["nick2"] =$chatroomtmp["nick2"];
                $chatroom["picture_nick1"] = $chatroomtmp["picture_nick1"];
                $chatroom["picture_nick2"] = $chatroomtmp["picture_nick2"];
                $chatroom["lastMessage"] = $chatroomtmp["lastMessage"];
                $chatroom["senderLastMessage"] = $chatroomtmp["senderLastMessage"];
                $chatroom["isReaded"] = $chatroomtmp["isReaded"];
                $chatroom["dateLastMessage"] = $chatroomtmp["dateLastMessage"];
                $chatroom["timeLastMessage"] = $chatroomtmp["timeLastMessage"]; 

                array_push($response["chatroom"], $chatroom);
            }
        }

        return $response;

        // $sql = "select * from chatroom where nick1 = '$nick'";

        /*
        $sql = "select * from chatroom where nick2 = '$nick'";
        //echo $sql;
        $result = $this -> conn -> query($sql);
        // $response = $this -> createArrayChatRoom($result, $response);
        $columns = $result -> fetchAll();
        if (count($columns) > 0){

            foreach($columns as $chatroomtmp){
                $chatroom = array();  

                $chatroom["chat_id"] = $chatroomtmp["chat_id"];
                $chatroom["nick1"] = $chatroomtmp["nick1"];
                $chatroom["nick2"] =$chatroomtmp["nick2"];
                $chatroom["picture_nick1"] = $chatroomtmp["picture_nick1"];
                $chatroom["picture_nick2"] = $chatroomtmp["picture_nick2"];
                $chatroom["lastMessage"] = $chatroomtmp["lastMessage"];
                $chatroom["senderLastMessage"] = $chatroomtmp["senderLastMessage"];
                $chatroom["isReaded"] = $chatroomtmp["isReaded"];
                $chatroom["dateLastMessage"] = $chatroomtmp["dateLastMessage"];
                $chatroom["timeLastMessage"] = $chatroomtmp["timeLastMessage"]; 

                array_push($response["chatroom"], $chatroom);
            }
        }
        */

    }

    // Obtengo las salas de chat solo de amigos (mas adelante)
    function getFriendsChatRoom($nick){
        // $response = array();
        $response["chatroom"] = array();

        $sql = "select * from chatroom where (nick1 = '$nick' and nick2 in (select nick2 from friends where nick1 = '$nick' ) ) 
        or (nick2 = '$nick' and nick1 in (select nick1 from friends where nick2 = '$nick' ) ) ;";


        // $sql = "select * from chatroom where nick1 = '$nick' and nick2 in (select nick2 from friends where nick1 = '$nick' )";
        $result = $this -> conn -> query($sql);
        $columns = $result -> fetchAll();
        if (count($columns) > 0){
            foreach($columns as $chatroomtmp){
                $chatroom = array();  

                $chatroom["chat_id"] = $chatroomtmp["chat_id"];
                $chatroom["nick1"] = $chatroomtmp["nick1"];
                $chatroom["nick2"] =$chatroomtmp["nick2"];
                $chatroom["picture_nick1"] = $chatroomtmp["picture_nick1"];
                $chatroom["picture_nick2"] = $chatroomtmp["picture_nick2"];
                $chatroom["lastMessage"] = $chatroomtmp["lastMessage"];
                $chatroom["senderLastMessage"] = $chatroomtmp["senderLastMessage"];
                $chatroom["isReaded"] = $chatroomtmp["isReaded"];
                $chatroom["dateLastMessage"] = $chatroomtmp["dateLastMessage"];
                $chatroom["timeLastMessage"] = $chatroomtmp["timeLastMessage"]; 

                array_push($response["chatroom"], $chatroom);
            }
        }
        
        return $response;

        /*
        $sql = "select * from chatroom where nick2 = '$nick' and nick1 in (select nick1 from friends where nick2 = '$nick' )";
        $result = $this -> conn -> query($sql);
        $columns = $result -> fetchAll();
        if (count($columns) > 0){
            foreach($columns as $chatroomtmp){
                $chatroom = array();  

                $chatroom["chat_id"] = $chatroomtmp["chat_id"];
                $chatroom["nick1"] = $chatroomtmp["nick1"];
                $chatroom["nick2"] =$chatroomtmp["nick2"];
                $chatroom["picture_nick1"] = $chatroomtmp["picture_nick1"];
                $chatroom["picture_nick2"] = $chatroomtmp["picture_nick2"];
                $chatroom["lastMessage"] = $chatroomtmp["lastMessage"];
                $chatroom["senderLastMessage"] = $chatroomtmp["senderLastMessage"];
                $chatroom["isReaded"] = $chatroomtmp["isReaded"];
                $chatroom["dateLastMessage"] = $chatroomtmp["dateLastMessage"]; 
                $chatroom["timeLastMessage"] = $chatroomtmp["timeLastMessage"]; 

                array_push($response["chatroom"], $chatroom);
            }
        }
        */


        // y cada query la debo añadir a un array $response['chatroom']
        // $response['chatroom']['nick'] = 


    }

    function createArrayChatRoom($result){
        $response["chatroom"] = array();
        $columns = $result -> fetchAll();
        if (count($columns) > 0){

            foreach($columns as $chatroomtmp){
                $chatroom = array();  

                // echo $chatroomtmp["nick1"];
                $chatroom["chat_id"] = $chatroomtmp["chat_id"];
                $chatroom["nick1"] = $chatroomtmp["nick1"];
                $chatroom["nick2"] =$chatroomtmp["nick2"];
                $chatroom["picture_nick1"] = $chatroomtmp["picture_nick1"];
                $chatroom["picture_nick2"] = $chatroomtmp["picture_nick2"];
                $chatroom["lastMessage"] = $chatroomtmp["lastMessage"];
                $chatroom["senderLastMessage"] = $chatroomtmp["senderLastMessage"];
                $chatroom["isReaded"] = $chatroomtmp["isReaded"];
                $chatroom["dateLastMessage"] = $chatroomtmp["dateLastMessage"];
                $chatroom["timeLastMessage"] = $chatroomtmp["timeLastMessage"]; 

                array_push($response["chatroom"], $chatroom);
            }

            // print_r($response);
            return $response;
        }
    }

    function getChatRoom($nick1, $nick2){

        // select * from chatroom where (nick1 = '$nick1' and nick2 = '$nick2') or (nick1 = '$nick2' and nick2 = '$nick1');

        $sql = "select * from chatroom where nick2 = '$nick2' and nick1 = '$nick1'";
        $result1 = $this -> conn -> query($sql);

        if ($result1 -> rowCount() == 0){
            $sql = "select * from chatroom where nick1 = '$nick2' and nick2 = '$nick1'";
            $result2 = $this -> conn -> query($sql);
            return $this -> createArrayChatRoom($result2);
        }
        else{
            return $this -> createArrayChatRoom($result1);
        }

    }


    function getUserProfile($nick){
        $sql = "select * from profile where nick = '$nick'";
        return $this -> conn -> query($sql);

    }

    function updateProfilePrueba($nick, $description = null, $genre = null, $image = null, $fechanacimiento = null, $nombre = null){
        $datos;
        if (!empty($fechanacimiento)){
            $datos["birthdate"] = $fechanacimiento;
        }
        if (!empty($description)){
            $datos["description"] = $description;
        }
        if (!empty($image)){
            $datos["picture"] = $image;
        }
        if (!empty($genre)){
            $datos["gender"] = $genre;
        }
        if (!empty($nombre)){
            $datos["name"] = $nombre;
        }

        // Recorro con foreach y hago consulta. Por cada elemento en $datos, un update de clave -> valor where nick = '$nick';
        foreach ($datos as $campotmp){
            $SQL = "UPDATE profile SET " . key($datos) . " = '" . $campotmp . "' where nick = '" . $nick . "'";
            next($datos);

            $this -> conn -> query($SQL);
        }
        return true;
    }

    /*
    function updateProfile($nick, $description = null, $genre = null, $image = null, $fechanacimiento = null, $nombre = null){
        // Ninguno vacío:
        if (!empty($description) && !empty($genre) && !empty($image) && !empty($fechanacimiento) && !empty($nombre)){
            $sql = "UPDATE profile SET birthdate = '" . $fechanacimiento . "', description = '" . $description .  "', picture = '"  
            . $image . "', gender = '" . $genre .  "', name='" . $nombre . "' where nick = '" . $nick . "'";
        }

        // Solo fecha de nacimiento
        else if (empty($description) && empty($genre) && empty($image) && !empty($fechanacimiento)){
            $sql = "UPDATE profile SET birthdate = '" . $fechanacimiento . "' where nick = '" . $nick . "'";
        }
        // Solo descripción
        else if (!empty($description) && empty($genre) && empty($image) && empty($fechanacimiento)){
            $sql = "UPDATE profile SET description = '" . $description . "' where nick = '" . $nick . "'";
        }
        // Solo imagen
        else if (empty($description) && empty($genre) && !empty($image) && empty($fechanacimiento)){
            $sql = "UPDATE profile SET picture = '" . $image . "' where nick = '" . $nick . "'";
        }
        // Solo genero
        else if (empty($description) && !empty($genre) && empty($image) && empty($fechanacimiento)){
            $sql = "UPDATE profile SET gender = '" . $genre . "' where nick = '" . $nick . "'";
        }
        // Fecha de nacimiento e imagen
        else if (empty($description) && empty($genre) && !empty($image) && !empty($fechanacimiento)){
            $sql = "UPDATE profile SET birthdate = '" . $fechanacimiento . "', picture = '"  
            . $image .  "' where nick = '" . $nick . "'";
        }
        // Fecha de nacimiento, imagen y descripción
        else if (!empty($description) && empty($genre) && !empty($image) && !empty($fechanacimiento)){
            $sql = "UPDATE profile SET birthdate = '" . $fechanacimiento . "', description = '" . $description .  "', picture = '"  
            . $image .  "' where nick = '" . $nick . "'";
        }
        // Fecha de nacimiento y genero
        else if (empty($description) && !empty($genre) && empty($image) && !empty($fechanacimiento)){
            $sql = "UPDATE profile SET birthdate = '" . $fechanacimiento . "', gender = '" . $genre .  "' where nick = '" . $nick . "'";
        }
        // Fecha de nacimiento, genero e imagen
        else if (empty($description) && !empty($genre) && !empty($image) && !empty($fechanacimiento)){
            $sql = "UPDATE profile SET birthdate = '" . $fechanacimiento . "', picture = '"  
            . $image . "', gender = '" . $genre .  "' where nick = '" . $nick . "'";        
        }
        // Fecha de nacimiento y descripcion
        else if (!empty($description) && empty($genre) && empty($image) && !empty($fechanacimiento)){
            $sql = "UPDATE profile SET birthdate = '" . $fechanacimiento . "', description = '" . $description .  "' where nick = '" . $nick . "'";
        }
        // Fecha de nacimiento, descripcion y genero
        else if (!empty($description) && !empty($genre) && empty($image) && !empty($fechanacimiento)){
            $sql = "UPDATE profile SET birthdate = '" . $fechanacimiento . "', description = '" . $description .  "', gender = '" . $genre .  "' where nick = '" . $nick . "'";
        }
        // Imagen y genero
        if (empty($description) && !empty($genre) && !empty($image) && empty($fechanacimiento)){
            $sql = "UPDATE profile SET picture = '"  . $image . "', gender = '" . $genre .  "' where nick = '" . $nick . "'";
        }
        // Imagen, genero y descripcion
        if (!empty($description) && !empty($genre) && !empty($image) && empty($fechanacimiento)){
            $sql = "UPDATE profile SET description = '" . $description .  "', picture = '"  
            . $image . "', gender = '" . $genre .  "' where nick = '" . $nick . "'";
        }
        // Imagen y descipcion
        if (!empty($description) && empty($genre) && !empty($image) && empty($fechanacimiento)){
            $sql = "UPDATE profile SET description = '" . $description .  "', picture = '"  
            . $image .  "' where nick = '" . $nick . "'";
        }
        // Descripcion y género
        if (!empty($description) && !empty($genre) && empty($image) && empty($fechanacimiento)){
            $sql = "UPDATE profile SET description = '" . $description . "', gender = '" . $genre .  "' where nick = '" . $nick . "'";
        }
        



        // echo $sql;

        if (!isset($sql)){
            $this -> error = "No hay consulta SQL";
            return false;
        }
        else {
            $this -> error = $sql;
            return $this -> conn -> query($sql);
        }



        //$query="UPDATE PROFILE SET ";
        //$query.=(!empty($_POST['input1']))? " a=$1,":"";
        //$query.=(!empty($_POST['input2']))? "b=$2,":"";
        //$query.=(!empty($_POST['input3']))? "c=$3,":"";
        //$query=substr($query,0,-1);
        //$query.="WHERE row = 'row_id'";


    }
    */

    
    function getFriendList($nick){
        $sql = "select nick, name, picture from user where nick in (select user1_nick from friends where user2_nick = '$nick') or nick in (select user2_nick from friends where user1_nick = '$nick')";
        return $this -> conn -> query($sql);

        // echo $SQL;

                // Obtengo los nicks de los amigos: 
        // select user1_nick from friends where user2_nick = 'lourdes';
        // select user2_nick from friends where user1_nick = 'lourdes';

    }


    function addFriendRequest($userWhoSendsRequest_nick, $userWhoReceivedRequest_nick){
        try{
            $sql = "select max(friendrequests_id) from friendrequests";
            $statement = $this -> conn -> query($sql);
            $columns = $statement -> fetchAll();
            $max = $columns[0]["max(friendrequests_id)"];
            $max = $max + 1;
            echo $max  . "</br>";

           // Primero compruebo si no existe ya esa friendrequests (con los 2 users)
           $sqlexists = "select count(*) from friendrequests where (userWhoReceivedRequest_nick = '$userWhoReceivedRequest_nick' and userWhoSendsRequest_nick= '$userWhoSendsRequest_nick') or (userWhoReceivedRequest_nick = '$userWhoSendsRequest_nick' and userWhoSendsRequest_nick= '$userWhoReceivedRequest_nick')";
           $columns = $this -> conn -> query($sqlexists) -> fetchAll();
           $numeroresultados = $columns[0]["count(*)"];
           echo "Numero de resultados: " . $numeroresultados;

            //$sql = "insert into friendrequests values(?, ?, ?, null, null);";
            if ($numeroresultados == 0){
                $sql2 = "insert into friendrequests values ('" . $max . "', '" . $userWhoSendsRequest_nick . "', '" . $userWhoReceivedRequest_nick . "', null, null)";
                echo $sql2;

                // $this -> conn -> prepare($sql) -> execute(array($max + 1, $userWhoSendsRequest_nick, $userWhoReceivedRequest_nick));
               $this -> conn -> query($sql2);
            }


           //Si no hay máximo da igual, porque select(max) dará 0
        }
        catch(PDOException $e){
            echo $e -> getMessage();
        }
    }

    function getFriendRequests($userWhoReceivedRequest_nick){
        $sql = "select * from friendrequests where userWhoReceivedRequest_nick = '" . $userWhoReceivedRequest_nick . "' and isAccepted is null";
        return $this -> conn -> query($sql);

    }

    function acceptFriendRequests($id){
        $sql = "update friendrequests set isAccepted = true where friendrequests_id = '" . $id . "'";
        $this -> conn -> query($sql);

    }

    function checkIfMyRequestsAreAccepted($userWhoSendsRequest_nick){
        $sql = "select * from friendrequests where userWhoSendsRequest_nick = '" . $userWhoSendsRequest_nick . "' and isAccepted = true";
        $statement = $this -> conn -> query($sql);


        // inserto en friends -> NO HACE FALTA, lo hará el trigger 
        // $this -> insertNewFriends($this -> getArrayOfFriendsToInsert($statement));

        // borro
        $this -> deleteFriendRequest($userWhoSendsRequest_nick);

        return $statement;
    }

    // borro de la tabla friendrequests
    function deleteFriendRequest($userWhoSendsRequest_nick){
        $this -> conn -> exec("delete from friendrequests where userWhoSendsRequest_nick = '" . $userWhoSendsRequest_nick . "' and isAccepted = true");
    }

    function deleteFriendRequestWithId($id){
        $this -> conn -> exec("delete from friendrequests where friendrequests_id = '" . $id . "'");
    }
    
    // obtengo lista de amigos
    function getArrayOfFriendsToInsert($statement){
        $columns = $statement -> fetchAll();

        $arrayFriends = array();
        if (count($columns) > 0){        
            for($i = 0; $i < count($columns); $i++){
                $arrayFriends[$i]["nick1"] = $columns[$i]["userWhoSendsRequest_nick"];
                $arrayFriends[$i]["nick2"] = $columns[$i]["userWhoReceivedRequest_nick"];
            }
        }
        return $arrayFriends;

    }

    // Inserta una lista de amigos
    function insertNewFriends($arrayFriends){
        foreach($arrayFriends as $friendtmp){
            $nick1 = $friendtmp["nick1"];
            $nick2 = $friendtmp["nick2"];
            $this -> conn -> query("insert into friends values ('" . $nick1 . "', '" . $nick2 . "')");
        }

    }


    function checkFriendRequestCanBeSent($nick1, $nick2){
        $sql = "SELECT * FROM friendrequests where (userWhoSendsRequest_nick = '" . $nick1 . "' and userWhoReceivedRequest_nick = '" . $nick2 . "') OR "
            . "(userWhoSendsRequest_nick  = '" . $nick2 . "' and userWhoReceivedRequest_nick = '" . $nick1 . "')";
        //echo $sql;

        $sqlFriends = "SELECT * FROM friends where (user1_nick = '" . $nick1 . "' and user2_nick = '" . $nick2 . "') OR " . "(user2_nick = '" . $nick1 . "' and user1_nick = '" . $nick2 . "')";
        //echo $sqlFriends;

        $pdo1 = $this -> conn -> query($sql);
        $count1 = $pdo1 -> rowCount();

        $pdo2 = $this -> conn -> query($sqlFriends);
        $count2 = $pdo2 -> rowCount();

        if ($count1 > 0 || $count2 > 0){
            return false; // No se puede mandar
        }
        else{
            return true; // Se pueden mandar
        }
    
    }


    function rateAnime($nick, $animeid, $rating){
        // TODO. Insert si no existe en ANIME_USER, update si ya existe

        // 1. Compruebo si existe en anime_user el nick del usuario con ese anime:
        $sqlExists = "select * from anime_user where anime_id = '" . $animeid . "' and user_id = (select user_id from user where nick= '" . $nick . "') ";
        $result = $this -> conn -> query($sqlExists);
        if ($result -> rowCount() > 0){
            // UPDATE
            $sqlUpdate = "update anime_user set rating_useranime = " . $rating . " where anime_id = '" . $animeid . "' and user_id = (select user_id from user where nick= '" . $nick . "') ";
            $this -> conn -> exec($sqlUpdate);
            echo $sqlUpdate;
        }
        else{
            // INSERT
            $sqlInsert = "insert into anime_user values ('" . $animeid . "',  (select user_id from user where nick= '" . $nick . "'), " . $rating . " )";
            $this -> conn -> exec($sqlInsert);
            echo $sqlInsert;
        }

        //  Update a la base de datos, la tabla ANIME_USER le pongo al usuario actual el nuevo rating (rating)
    }


    function updateAnime2($id, $picture, $name, $description){
        $SQL = "update anime3 set picture = '$picture', description = '$description' where anime_id = '$id'";
        $this -> conn -> query($SQL);
        if ($name != "null"){
           $SQL2 = "update anime3 set name = '$name' where anime_id = '$id'";
           $this -> conn -> query($SQL2);
        }

        echo $SQL;
    }

    function deleteFriend($yourNick, $friendNick){
        $sql = "delete from friends where user1_nick = '" . $yourNick . "' and user2_nick = '" . $friendNick . "'";
        $sql2 = "delete from friends where user2_nick = '" . $yourNick . "' and user1_nick = '" . $friendNick . "'";
        $this -> conn -> query($sql);
        $this -> conn -> query($sql2);

        $this -> conn -> exec("delete from friendrequests where userWhoSendsRequest_nick = '$yourNick' and userWhoReceivedRequest_nick = '$friendNick' ");
        $this -> conn -> exec("delete from friendrequests where userWhoReceivedRequest_nick = '$yourNick' and userWhoSendsRequest_nick = '$friendNick' ");
    }

    function userIsRegistered($email){
        $sql = "select * from user where email = '" . $email . "'";
        $result = $this -> conn -> query($sql);

        if ($result -> rowCount() > 0){
            return true;
        }
        else
            return false;
    }

    function getUserFromEmail($email){
        $sql = "select * from profile where email = '$email'";
        return $this -> conn -> query($sql);

    }

    function getIfThisNickExists($nick){
        $sql = "select * from user where nick = '" . $nick . "'";
        $result = $this -> conn -> query($sql);

        if ($result -> rowCount() > 0){
            return true;
        }
        else
            return false;
    }

    function insertUserFirebase($nick, $email){
        $sql = "SELECT MAX(CAST(user_id AS SIGNED)) FROM user";
        $columns = $this -> conn -> query($sql) -> fetchAll();
        $maxId = $columns[0]["MAX(CAST(user_id AS SIGNED))"];
        $maxId++;

        try{
            $sqlInsert = "insert into user values (?, ?, ?, ?, ?, ?, ?)";
            $statement = $this -> conn -> prepare($sqlInsert);
            if (empty($phone))
                $phone = null;
            if (empty($email))
                $email = null;

            $imagesDir = '/home/marina/animens/imagenes/rndprofile/';
            $images = glob($imagesDir . '*.{jpg,jpeg,png,gif}', GLOB_BRACE);
            $randomImage = $images[array_rand($images)]; 
            $randomImageUrl = "https://marina.edufdezsoy.es/" . substr($randomImage, 13);
            echo $randomImageUrl;

            $statement -> execute(array($maxId, $nick, null, null, null, $email, $randomImageUrl)); // EL ULTIMO ES PICTURE Y NO PUEDE SER NULL
        }
        catch(PDOException $e){
            echo $e -> getMessage();
        }

    }

    function deleteUser($email){
        $sql = "delete from profile where email = '$email'; delete from user where email = '$email';";
        $this -> conn -> exec($sql);
    }
	
	function updateLastChatMessage($lastMessage, $timeLastMessage, $dateLastMessage, $senderLastMessage, $chat_id){
		$sql = "update chatroom set lastMessage = '$lastMessage', timeLastMessage = '$timeLastMessage', dateLastMessage = '$dateLastMessage', senderLastMessage = '$senderLastMessage', isReaded = 0 where chat_id = '$chat_id'";
		$this -> conn -> exec($sql);
	}
	
	function setLastMessageReaded($chat_id){
		$sql = "update chatroom set isReaded = true where chat_id = '$chat_id'";
		$this -> conn -> exec($sql);
    }
    
    function getUnreadedMessages($nick){
        // con esto debe funcionar porque senderLastMessage si es null no lo obtendría y los que están sin mensajes no los obtiene
        $sql = "select * from chatroom where (nick1 = '$nick' and isReaded = 0 and senderLastMessage != '$nick') or (nick2 = '$nick' and isReaded = 0 and senderLastMessage != '$nick') ";
        $result = $this -> conn -> query($sql);
        
        $response = array();
        $response["chatroom"] = array();
        $columns = $result -> fetchAll();
        if (count($columns) > 0){
            foreach($columns as $chatroomtmp){
                $chatroom = array();  
                $chatroom["chat_id"] = $chatroomtmp["chat_id"];
                $chatroom["nick1"] = $chatroomtmp["nick1"];
                $chatroom["nick2"] =$chatroomtmp["nick2"];
                $chatroom["picture_nick1"] = $chatroomtmp["picture_nick1"];
                $chatroom["picture_nick2"] = $chatroomtmp["picture_nick2"];
                $chatroom["lastMessage"] = $chatroomtmp["lastMessage"];
                $chatroom["senderLastMessage"] = $chatroomtmp["senderLastMessage"];
                $chatroom["isReaded"] = $chatroomtmp["isReaded"];
                $chatroom["dateLastMessage"] = $chatroomtmp["dateLastMessage"];
                $chatroom["timeLastMessage"] = $chatroomtmp["timeLastMessage"]; 

                array_push($response["chatroom"], $chatroom);
            }
        }

        return $response;
    }


    function getImageRandom(){
        $imagesDir = '/home/marina/animens/imagenes/rndprofile/';
        $images = glob($imagesDir . '*.{jpg,jpeg,png,gif}', GLOB_BRACE);
        $randomImage = $images[array_rand($images)]; 

        $randomImageUrl = "https://marina.edufdezsoy.es/" . substr($randomImage, 13);
        return $randomImageUrl;
    }


	function insertReport($nickUserReported, $nickUserWhoReports, $imagePrueba, $motivos, $descripcion){
		// la imagen es opcional
		if (empty($imagePrueba))
			$imagePrueba = null;
		
		$sql = "insert into reportes (nickUserReported, nickUserWhoReports, imagePrueba, motivos, descripcion) 
			values ('$nickUserReported', '$nickUserWhoReports', '$imagePrueba', '$motivos', '$descripcion')";

		$this -> conn -> query($sql);
		
	}
    

}

?>