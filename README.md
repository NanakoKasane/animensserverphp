<p>Parte del servidor en PHP del proyecto <i><b>Animens</b></i></p>

<p>La parte del <b>cliente</b> está en Android (Java) y se puede encontrar en el siguiente repositorio: <i>https://gitlab.com/NanakoKasane/proyectoanimens</i> </p>

<br/>
<p>Las diferentes consultas a la base de datos MySQL se encuentran en el fichero <b><i>dao.php</i> </b></p>
<br/>