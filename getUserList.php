<?php
header("Content-Type: application/json; charset=UTF-8");
include ("dao.php");
$dao = new DAO();

// todos los usuarios
if (!$_GET){

    $result = $dao -> getAllUsers();
    $columns = $result -> fetchAll();

    showUsers($columns);
}

function showUsers($columns){
    $users["users"] = array(); 

    if (count($columns) > 0){
        for($i = 0; $i < count($columns); $i++){
            $user = array();
            $user["nick"] = $columns[$i]["nick"];
            $user["name"] = $columns[$i]["name"];
            $user["picture"] = $columns[$i]["picture"];

            array_push($users["users"], $user);

        }    
        echo json_encode($users, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);

    }
}


// Si le pasamos por GET -> ?nick=minick    -> hacemos la consulta de obtener los que tienen más animes en común a menos.
if ($_GET){
    if (isset($_GET["nick"])){
        $nick = $_GET["nick"];

        $users = $dao -> getUsersOrderByAnimeFavorites($nick);
        echo json_encode($users, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);

    }

}

?>