<?php
include ("dao.php");
$dao = new DAO();

if ($_GET){
    if (isset($_GET["nick"]) && isset($_GET["animeid"]) && isset($_GET["rating"])){
        $nick = $_GET["nick"];
        $animeid = $_GET["animeid"];
        $rating = $_GET["rating"];

        $dao -> rateAnime($nick, $animeid, $rating);
        echo "Success GET";
    }
}

if ($_POST){
    if (isset($_POST["nick"]) && isset($_POST["animeid"]) && isset($_POST["rating"])){
        $nick = $_POST["nick"];
        $animeid = $_POST["animeid"];
        $rating = $_POST["rating"];

        $dao -> rateAnime($nick, $animeid, $rating);
        echo "Success POST";
    }
}



?>