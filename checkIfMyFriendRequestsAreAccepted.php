<?php
header("Content-Type: application/json; charset=UTF-8");
include ("dao.php");

$dao = new DAO();

if ($_GET){
    if (isset($_GET["userWhoSendsRequest_nick"])){
        $user = $_GET["userWhoSendsRequest_nick"];
        $resultset = $dao -> checkIfMyRequestsAreAccepted($user);


        $columns = $resultset -> fetchAll();

        if (count($columns) > 0){
            $response["friendrequests"] = array();
        
            foreach($columns as $friendrequesttmp){
                $friendrequest = array();  
        
                $friendrequest["friendrequests_id"] = $friendrequesttmp["friendrequests_id"];
                $friendrequest["userWhoSendsRequest_nick"] = $friendrequesttmp["userWhoSendsRequest_nick"];
                $friendrequest["userWhoReceivedRequest_nick"] = $friendrequesttmp["userWhoReceivedRequest_nick"];
                $friendrequest["isAccepted"] = $friendrequesttmp["isAccepted"];
                $friendrequest["picture"] = $friendrequesttmp["picture"];

                // como un add
                array_push($response["friendrequests"], $friendrequest);
            }
        
        
            echo json_encode($response, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);


            // inserto en friends
            // $dao -> insertNewFriends($dao -> getArrayOfFriendsToInsert($resultset));

            // borro
            // $dao -> deleteFriendRequest($user);
        
        }
        else{
            echo "No hay peticiones aceptadas";
        }

    }

}

?>