<?php
header("Content-Type: application/json; charset=UTF-8");
include ("dao.php");

$dao = new DAO();

if ($_GET){
    if (isset($_GET["nick"])){
        $nick = $_GET["nick"];

        $result = $dao -> getUserProfile($nick);
        $columns = $result -> fetchAll();

        if (count($columns) > 0){
            $response["profile"] = array();
        
            foreach($columns as $profiletmp){
                $profile = array();  

                $profile["name"] = $profiletmp["name"];
                $profile["email"] = $profiletmp["email"];
                $profile["phonenumber"] = $profiletmp["phonenumber"];
                $profile["address"] = $profiletmp["address"];
                $profile["address"] = $profiletmp["address"];
                $profile["birthdate"] = $profiletmp["birthdate"];
                $profile["gender"] = $profiletmp["gender"];
                $profile["description"] = $profiletmp["description"];
                $profile["nick"] = $profiletmp["nick"];
                $profile["picture"] = $profiletmp["picture"];

                $profile["registerDate"] = $profiletmp["registerdate"];

                // como un add
                array_push($response["profile"], $profile);
            }
        
        
            echo json_encode($response, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);
        }


    }
}



?>