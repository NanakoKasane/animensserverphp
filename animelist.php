<?php
header("Content-Type: application/json; charset=UTF-8");
include ("dao.php");

$dao = new DAO();

$resultset = $dao -> getAnimes();
//for($i = 0; $i < $resultset -> columnCount(); $i++){
//    echo($resultset -> getColumnMeta($i)["name"]);
//    echo "<br/>";
//}

$columns = $resultset -> fetchAll();

if (count($columns) > 0){
    $response["animes"] = array();

    foreach($columns as $animetmp){
        $anime = array();  
       // $anime["anime"][] = $animetmp;

        $anime["anime_id"] = $animetmp["anime_id"];
        $anime["name"] = htmlspecialchars_decode($animetmp["name"], ENT_QUOTES);
        $anime["genre"] = $animetmp["genre"];
        $anime["type"] = $animetmp["type"];
        $anime["episodes"] = $animetmp["episodes"];
        $anime["rating"] = $animetmp["rating"];
        $anime["picture"] = $animetmp["picture"];
        $anime["anime_idIncrement"] = $animetmp["anime_idIncrement"];

        // como un add
        array_push($response["animes"], $anime);
    }


    echo json_encode($response, JSON_UNESCAPED_UNICODE|JSON_PRETTY_PRINT);

}


?>